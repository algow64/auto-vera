const BASE_URL = 'https://spanint.kemenkeu.go.id/spanint/data/';
const ROUTER = 'https://spanint.kemenkeu.go.id/spanint/latest/#span/';

const TASKS = {
    pfk: {
        satker_akun: {
            name: 'PFK salah satker/akun',
            method: 'GET',
            url: 'DataPFK/pfk_salah_satker'
        }, potong: {
            name: 'PFK salah potong',
            method: 'GET',
            url: 'DataPFK/pfk_salah_potong'
        }, jendok: {
            name: 'PFK salah jendok',
            method: 'GET',
            url: 'DataPFK/pfk_salah_jendok'
        }, pecahan: {
            name: 'PFK salah pecahan',
            method: 'GET',
            url: 'DataPFK/pfk_salah_pecahan'
        }
    },
    suspend: {
        pengembalian: {
            name: 'Suspend pengembalian belanja',
            method: 'GET',
            url: 'dataSPM/NmSatkerSuspend'
        }, penerimaan: {
            name: 'Suspend penerimaan',
            method: 'POST',
            form: {
                'kdkppn': null,
                'koreksi': 'BELUM',
                'tgl_awal': '01-01-2019',
                'tgl_akhir': null,
                'submit_file': null
            },
            url: 'dataGR/SuspendSatkerPenerimaan'
        }, akun: {
            name: 'Suspend akun',
            method: 'POST',
            form: {
                'kdkppn': null,
                'koreksi': 'BELUM',
                'tgl_awal': '01-01-2019',
                'tgl_akhir': null,
                'submit_file': null
            },
            url: 'dataGR/SuspendAkunPenerimaan'
        }
    },
    etc: {
        retur: {
            name: 'Retur SP2D',
            method: 'POST',
            form: {
                'kdkppn': null,
                'nosp2d': null,
                'notransaksi': null,
                'kdsatker': null,
                'status': 'BELUM PROSES',
                'tgl_awal': '01-01-2019',
                'tgl_akhir': null,
                'tgl_setor_awal': null,
                'tgl_setor_akhir': null,
                'tgl_pengganti_awal': null,
                'tgl_pengganti_akhir': null,
                'submit_file': null
            },
            url: 'dataRetur/monitoringRetur2'
        }, hold: {
            name: 'Hold invoice',
            method: 'POST',
            form: {
                'kdkppn': null,
                'invoice': null,
                'STATUS': 1,
                'submit_file': null
            },
            url: 'dataSPM/HoldSPM'
        }
    }
};

const AUTH = {
    username: 'user',
    password: 'password',
    ta: '2019'
};

module.exports = { BASE_URL, ROUTER, TASKS, AUTH };