function Header() {
    let data = {
        auth: {
            headers: { 'Accept': 'application/json'}
        },
        inner: {
            headers: { 
                'Accept': 'application/json',
                'Authorization': undefined,
                'Connection': 'keep-alive'
            }
        }
    };

    this.getHeader = (jwt = null) => {
        // Akses endpoint perlu auth?
        let key = 'auth';
        if(jwt) {
            key = 'inner';
            data.inner.headers['Authorization'] = `Bearer ${jwt}`;
        }
        return data[key];
    } 
}

module.exports = new Header;