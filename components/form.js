const querystring = require('querystring');
const Params = require('./params');

function FormData() {
    this.getFormData = (task, sub = null) => {
        // form data untuk auth
        let parsedData = querystring.stringify(data[task]);        
        if(sub) {
            // form data untuk non auth
            let parameters = data[task][sub].form;
            parameters.tgl_akhir = today();
            parsedData = querystring.stringify(data[task][sub].form);
        }
        return parsedData;
    }

    let data = {
        auth: Params.AUTH,
        suspend: JSON.parse(JSON.stringify(Params.TASKS.suspend)),
        etc: JSON.parse(JSON.stringify(Params.TASKS.etc))
    };

    const today = () => {
        const time = new Date();
        let date = String(time.getDate());
        let month = String(time.getMonth() + 1);
        const year = String(time.getFullYear());

        if(date.length === 1){
            date = '0' + date;
        }
        if(month.length === 1){
            month = '0' + month;
        }

        return `${date}-${month}-${year}`;
    }
}

module.exports = new FormData;