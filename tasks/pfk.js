const Params = require('../components/params');
const Request = require('../http/request');

const pfk = async (jwt) => {
    let pfkParams = Object.values(Params.TASKS.pfk);
    let promisedResults = [];

    pfkParams.forEach( (element) => {
        promisedResults.push(Request.get(jwt, element));
    });
    const resolvedPromise = await Promise.all(promisedResults);
    return resolvedPromise;
};

module.exports = pfk;