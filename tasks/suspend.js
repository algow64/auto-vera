const Params = require('../components/params');
const Request = require('../http/request');

const suspend = async (jwt) => {
    const suspendData = Params.TASKS.suspend;
    let suspendKeys = Object.keys(suspendData);
    let promisedResults = [];

    suspendKeys.forEach( (subTaskName) => {
        let subTaskParams = suspendData[subTaskName];

        if(subTaskParams.method === 'POST') {
            promisedResults.push(Request.post(jwt, 'suspend', subTaskName, subTaskParams));
        } else {
            promisedResults.push(Request.get(jwt, subTaskParams));
        }
    });

    const resolvedPromise = await Promise.all(promisedResults);
    return resolvedPromise;
}

module.exports = suspend;