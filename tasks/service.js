const axios = require('axios');
const Pfk = require('./pfk');
const Suspend = require('./suspend');
const Etc = require('./etc');

const service = (jwt) => {
    let listOfTasks = [Pfk(jwt), Suspend(jwt), Etc(jwt)];

    return new Promise( (resolve, reject) => {
        axios.all(listOfTasks).then(axios.spread( (pfkData, suspendData, etcData) => {
            const mailData = [...pfkData, ...suspendData, ...etcData];
            resolve(mailData);
        })).catch( (e) => {
            reject(e);
        });
    });
};

module.exports = service;