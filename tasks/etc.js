const Params = require('../components/params');
const Request = require('../http/request');

const etc = async (jwt) => {
    const returData = Params.TASKS.etc;
    let returKeys = Object.keys(returData);
    let promisedResults = [];

    returKeys.forEach( (subTaskName) => {
        let subTaskParams = returData[subTaskName];

        if(subTaskParams.method === 'POST') {
            promisedResults.push(Request.post(jwt, 'etc', subTaskName, subTaskParams));
        } else {
            promisedResults.push(Request.get(jwt, subTaskParams));
        }
    });

    const resolvedPromise = await Promise.all(promisedResults);
    return resolvedPromise;
}

module.exports = etc;