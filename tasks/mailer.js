const nodemailer = require('nodemailer');
const Params = require('../components/params');

const notify = (data) => {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'email',
            pass: 'password'
        }
    });

    let message = generate(data);

    const mailOptions = {
        from: 'email',
        to: 'email, email',
        subject: 'Notifikasi OMSPAN',
        html: message
    };

    return new Promise( (resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if(err) {
                reject(err.response);
            } else {
                resolve(`Notification sent to ${JSON.stringify(info.envelope.to)}`);
            }
        });
    }); 
}

let generate = (messageData) => {
    let head = '<h1>Notifikasi OMSPAN</h1><br/><br/>';
    let body = messageData.map( (data) => {
        return `<h2>${data.name}</h2>
                <p>Terdapat ${data.value} data.</p>
                <p>Cek <a href=${Params.ROUTER + data.url}>di sini</a><br/>`;
    });

    return head + body;
}

module.exports = notify;