const axios = require('axios');
const Params = require('../components/params');
const FormData = require('../components/form');
const Header = require('../components/header');

const auth = () => {
    const headerData = Header.getHeader();
    const authFormData = FormData.getFormData('auth');

    return new Promise((resolve, reject) => {
        axios.post(`${Params.BASE_URL}auth/requestJWTToken`, authFormData, headerData).then((response) => {
            console.log('Logged In!');
            resolve(response.data.data.token);
        }).catch((e) => {
            reject(e.response.data);
        });
    });
}

module.exports = auth;