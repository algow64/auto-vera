const axios = require('axios');
const Params = require('../components/params');
const Header = require('../components/header');
const FormData = require('../components/form');

function Request () {
    this.post = (jwt, taskName, subTaskName, subTaskParams) => {
        const url = Params.BASE_URL + subTaskParams.url;
        const formData = FormData.getFormData(taskName, subTaskName);
        const headerData = Header.getHeader(jwt);

        return new Promise( (resolve, reject) => {
            axios.post(url, formData, headerData).then( (response) => {
                const notificationData = verify(subTaskParams, response.data.data.body);
                console.log(`${subTaskParams.name} fetched with ${notificationData.value} results`);
                resolve(notificationData);
            }).catch( (e) => {
                reject(e.code);
            })
        });
    }

    this.get = (jwt, subTaskParams) => {
        const url = Params.BASE_URL + subTaskParams.url;
        const headerData = Header.getHeader(jwt);

        return new Promise( (resolve, reject) => {
            axios.get(url, headerData).then( (response) => {
                const notificationData = verify(subTaskParams, response.data.data.body);
                console.log(`${subTaskParams.name} fetched with ${notificationData.value} results`);
                resolve(notificationData);
            }).catch( (e) => {
                reject(e.code);
            })
        });
    }

    const verify = ({ name, url }, requestData) => {
        let notifData = {
            name,
            url,
            value: requestData.length
        }

        if(String(requestData[0][0].value).toUpperCase() === 'TIDAK ADA DATA') {
            notifData.value = 0;
        }

        return notifData;
    }
}

module.exports = new Request;