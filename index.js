process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const Auth = require('./http/auth');
const Service = require('./tasks/service');
const SendEmail = require('./tasks/mailer');

( async () => {
    try {
        const jwt = await Auth();
        const messageData = await Service(jwt);
        const notification = await SendEmail(messageData);
        console.log(notification);
    } catch (error) {
        console.log(error);
    }
}) ();